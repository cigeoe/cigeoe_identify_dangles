# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CIGeoEIdentifyDangles
                                 A QGIS plugin
 Identifies dangles in a viewport
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2019-01-23
        git sha              : $Format:%H$
        copyright            : (C) 2019 by CIGeoE Identify Dangles
        email                : igeoe@igeoe.pt
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtWidgets import QAction, QToolBar, QMessageBox


from qgis.gui import *
from qgis.core import *

# Initialize Qt resources from file resources.py
from .resources import *
from .Retangulo import Retangulo


# Import the code for the dialog
from .CIGeoE_Identify_Dangles_dialog import CIGeoEIdentifyDanglesDialog
import os.path


class CIGeoEIdentifyDangles:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'CIGeoEIdentifyDangles_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&CIGeoE Identify Dangles')

        cigeoeToolBarExists = False
        for x in iface.mainWindow().findChildren(QToolBar): 
            if x.windowTitle() == 'CIGeoE':
                self.toolbar = x
                cigeoeToolBarExists = True
        if cigeoeToolBarExists==False:
            self.toolbar = self.iface.addToolBar(u'CIGeoE')
           
        self.toolbar.setObjectName(u'CIGeoEIdentifyDangles')


        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        self.first_start = None

        #*****************************************************
        self.totPoints=[]

        self.selectedFeats=[]
        self.canvas = self.iface.mapCanvas()
        self.pluginIsActive = False

        #************* connect to the rectangle ******************             
     
        self.retangulo = Retangulo(self.canvas, self.iface, self)         #add iface for work with active layer 
        self.retangulo.totPoints=[]            # contain the markers to removed when disabling the plugin

        self.rectIsActive = False
       
        self.viewportIsActive = False

        #*********************************************



    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('CIGeoEIdentifyDangles', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """
        

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            #self.iface.addToolBarIcon(action)
            self.toolbar.addAction(action)                             #++

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/CIGeoE_Identify_Dangles/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'CIGeoE Identify Dangles : Identifies dangles in a viewport'),
            callback=self.run,
            parent=self.iface.mainWindow())

        # will be set False in run()
        self.first_start = True


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&CIGeoE Identify Dangles'),
                action)
            self.iface.removeToolBarIcon(action)

            # remove the toolbar
            del self.toolbar

    #************************************************

    def onClosePlugin(self):
        """Cleanup necessary items here when plugin is closed"""
        
        self.selectedFeats = None
        self.pluginIsActive = False
               
        self.rectIsActive = False        
        self.viewportIsActive = False

        # makers to remove
        if len(self.retangulo.totPoints) > 0:
            for ver in self.retangulo.totPoints:
                self.iface.mapCanvas().scene().removeItem(ver)
            self.retangulo.totPoints = []

        # Unset the map tool in case it's set
        self.iface.mapCanvas().unsetMapTool(self.retangulo)


    #********************************************************************


    def run(self):
        """Run method that performs all the real work"""
       
        layers = self.iface.mapCanvas().layers() 
        if not layers:        
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer is not loaded!')            
            return 

        #if not self.pluginIsActive:
        if self.pluginIsActive == False:     
            self.pluginIsActive = True
             
            reply = QMessageBox.question(self.iface.mainWindow(), 'Identify Dangles', 'Yes (draw rectangle to identify)  or  No (identify in viewport) ', QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:                               
                if self.rectIsActive == False:                    
                    self.iface.mapCanvas().setMapTool(self.retangulo)
                    self.rectIsActive = True
                    
        
            if reply == QMessageBox.No:
                #select all features in view, in visible layers
                viewportPolygon = QgsGeometry().fromWkt(self.iface.mapCanvas().extent().asWktPolygon())
            
                layers = self.iface.mapCanvas().layers() 
                self.selectedFeats=[]

                #iterate all layers
                for layer in layers:       
                    #consider only vector layers with and Line geometry                    
                    if layer.type() != QgsMapLayer.VectorLayer or layer.geometryType() != QgsWkbTypes.LineGeometry:  
                        continue
                    
                    inViewFeats = layer.getFeatures(QgsFeatureRequest().setFilterRect(viewportPolygon.boundingBox()))
                        
                    r = viewportPolygon.boundingBox()
                    for feat in inViewFeats:              
                        self.selectedFeats.append(feat)
                
                if(len(self.selectedFeats) !=0): 
                    selectPoint = []
                    for elem in self.selectedFeats:
                        geom = elem.geometry().asPolyline()                         

                        start_point = QgsPointXY(geom[0])
                        end_point = QgsPointXY(geom[-1])
                                                
                        #start_point = end_point (equal points does not consider)  
                        if (start_point.x() == end_point.x()):
                                if (start_point.y() == end_point.y()) :
                                    continue 

                        #checks if the start_point is inside the box
                        if (r.xMinimum() <= start_point.x() <= r.xMaximum()) and (r.yMinimum() <= start_point.y() <= r.yMaximum()) :
                            flag = 0                            
                            point = QgsGeometry.fromPointXY(start_point)    

                            for elem1 in self.selectedFeats:  
                                if elem1 != elem:                                              
                                    a = QgsPoint(elem1.geometry().asPolyline()[0])
                                    b = QgsPoint(elem1.geometry().asPolyline()[-1])
                                    
                                    if ( isClose(start_point.x(), a.x()) and isClose(start_point.y(), a.y()) ) or ( isClose(start_point.x(), b.x()) and isClose(start_point.y(), b.y()) ) :
                                        flag = 1         # intersects 
                                

                            if flag == 0:
                                selectPoint.append(start_point) 

                        if (r.xMinimum() <= end_point.x() <= r.xMaximum()) and (r.yMinimum() <= end_point.y() <= r.yMaximum()) :
                            flag = 0                            
                            point = QgsGeometry.fromPointXY(end_point)    
                            for elem1 in self.selectedFeats: 
                                if elem1 != elem:                                           
                                    a = QgsPoint(elem1.geometry().asPolyline()[0])
                                    b = QgsPoint(elem1.geometry().asPolyline()[-1])
                                    
                                    if ( isClose(end_point.x(), a.x()) and isClose(end_point.y(), a.y()) ) or ( isClose(end_point.x(), b.x()) and isClose(end_point.y(), b.y()) ) :
                                        flag = 1 
 
                            if flag == 0:                        
                                selectPoint.append(end_point)      
      
                    
                    for i in range(len(selectPoint)):
                        # draw a marker       
                        point = QgsVertexMarker(self.canvas)                       
                        point.setCenter(QgsPointXY(selectPoint[i][0], selectPoint[i][1]))       # coordinate x, y
                        point.setColor(QColor(255,0, 0))      #(R,G,B)
                        point.setIconSize(10)
                        point.setIconType(QgsVertexMarker.ICON_CIRCLE)
                        point.setPenWidth(3)
                
                        point.show()            # to show a marker

                        self.totPoints.append(point)
                    
                self.iface.mapCanvas().refresh()

        else:   
            # remove all marker            
            if len(self.totPoints) > 0:
                for ver in self.totPoints:
                    self.iface.mapCanvas().scene().removeItem(ver)
                self.totPoints = []

            self.onClosePlugin()
            self.iface.mapCanvas().refresh()

def isClose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
            

