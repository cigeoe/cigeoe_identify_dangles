# -*- coding: utf-8 -*-

from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QAction, QToolBar, QMessageBox 

from qgis.gui import QgsMapToolEmitPoint, QgsRubberBand, QgsVertexMarker
from qgis.core import QgsWkbTypes, QgsPointXY, QgsPoint, QgsRectangle, QgsFeatureRequest, QgsMapLayer, Qgis, QgsGeometry
from qgis.utils import iface 

class Retangulo(QgsMapToolEmitPoint):         
    
    def __init__(self, canvas, iface, identifyDanglesMain):
        
        identifyDanglesMain=[]

        """
        draws a rectangle, then features that intersect this rectangle are added to selection 
        """
        # Store reference to the map canvas
        self.canvas = canvas  

        self.iface = iface        # add iface for work with active layer   

        self.totPoints = []   
            
                
        QgsMapToolEmitPoint.__init__(self, self.canvas)        
        
        self.rubberBand = QgsRubberBand(self.canvas, geometryType=QgsWkbTypes.LineGeometry)
        self.rubberBand.setColor(QColor(0, 0, 240, 100))
        self.rubberBand.setWidth(1)
        
        self.reset() 
         

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False        
        self.rubberBand.reset( QgsWkbTypes.PolygonGeometry )

    
    def canvasPressEvent(self, e):
        """
        Method used to build rectangle if shift is held, otherwise, feature select/deselect and identify is done.
        """
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        
        self.showRect(self.startPoint, self.endPoint)
        
    #self.isEmittingPoint is used to build the rubberband on the method on CanvasMoveEvent
            
    def canvasMoveEvent(self, e):
        """
        Used only on rectangle select.
        """
        if not self.isEmittingPoint:
            return
        self.endPoint = self.toMapCoordinates( e.pos() )
        self.showRect(self.startPoint, self.endPoint)

   
    def showRect(self, startPoint, endPoint):
        """
        Builds rubberband rect.
        """        
        self.rubberBand.reset( QgsWkbTypes.PolygonGeometry )
        if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
            return
        
        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(startPoint.x(), endPoint.y())
        point3 = QgsPointXY(endPoint.x(), endPoint.y())
        point4 = QgsPointXY(endPoint.x(), startPoint.y())

        self.rubberBand.addPoint(point1, False)
        self.rubberBand.addPoint(point2, False)
        self.rubberBand.addPoint(point3, False)
        self.rubberBand.addPoint(point4, True)    # true to update canvas
        self.rubberBand.show()


    def rectangle(self):
        """
        Builds rectangle from self.startPoint and self.endPoint
        """
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None
        return QgsRectangle(self.startPoint, self.endPoint)

    
    def canvasReleaseEvent(self, e):
        
        """
        After the rectangle is built, here features are selected.
        """
        self.isEmittingPoint = False

        rect = self.rectangle()            #QgsRectangle
        
        if rect is not None:
            # remove all marker             
            if len(self.totPoints) > 0:                
                for ver in self.totPoints:
                    self.iface.mapCanvas().scene().removeItem(ver)
                self.totPoints = [] 

                self.iface.mapCanvas().refresh()

            
            rectSelFeats=[]
            
            layers = self.iface.mapCanvas().layers() 
            
            for layer in layers:       #iterate all layers             
                if layer.type() != QgsMapLayer.VectorLayer or layer.geometryType() != QgsWkbTypes.LineGeometry:  
                    continue

                r = self.canvas.mapSettings().mapToLayerCoordinates(layer, rect)
                                           
                inRectangle = layer.getFeatures(QgsFeatureRequest().setFilterRect(r))                  
                for feat in inRectangle:     
                    rectSelFeats.append(feat) 
                                
            
            if(len(rectSelFeats) !=0): 
                selectPoint = []
                for elem in rectSelFeats:                    
                    el = elem.geometry().asPolyline()

                    start_point = QgsPointXY(el[0])
                    end_point = QgsPointXY(el[-1])

                    #start_point = end_point (equal points does not consider)  
                    if (start_point.x() == end_point.x()):
                        if (start_point.y() == end_point.y()) :
                            continue 
                                  
                    if (r.xMinimum() <= start_point.x() <= r.xMaximum()) and (r.yMinimum() <= start_point.y() <= r.yMaximum()) :
                        flag = 0                        
                        point = QgsGeometry.fromPointXY(start_point)    
                        for elem1 in rectSelFeats:  
                            if elem1 != elem:                                            
                                a = QgsPoint(elem1.geometry().asPolyline()[0])
                                b = QgsPoint(elem1.geometry().asPolyline()[-1])
                                if ( isClose(start_point.x(), a.x()) and isClose(start_point.y(), a.y()) ) or ( isClose(start_point.x(), b.x()) and isClose(start_point.y(), b.y()) ) :
                                    flag = 1       # intersects
                        
                        if flag == 0:
                            selectPoint.append(start_point) 
                        
                    if (r.xMinimum() <= end_point.x() <= r.xMaximum()) and (r.yMinimum() <= end_point.y() <= r.yMaximum()) :
                        flag = 0
                        point = QgsGeometry.fromPointXY(end_point)    
                        for elem1 in rectSelFeats: 
                            if elem1 != elem:                                           
                                a = QgsPoint(elem1.geometry().asPolyline()[0])
                                b = QgsPoint(elem1.geometry().asPolyline()[-1])
                                if ( isClose(end_point.x(), a.x()) and isClose(end_point.y(), a.y()) ) or ( isClose(end_point.x(), b.x()) and isClose(end_point.y(), b.y()) ) :
                                    flag = 1 

                        if flag == 0:                        
                            selectPoint.append(end_point)         
                
                for i in range(len(selectPoint)):
                    point = QgsVertexMarker(self.canvas)                    
                    
                    point.setCenter(QgsPointXY(selectPoint[i][0], selectPoint[i][1]))    
                    point.setColor(QColor(255,0, 0)) #(R,G,B)
                    point.setIconSize(10)
                    point.setIconType(QgsVertexMarker.ICON_CIRCLE)
                    point.setPenWidth(3)
                
                    point.show() # to show a marker

                    self.totPoints.append(point)

            self.rubberBand.hide()
            self.iface.mapCanvas().refresh()
            identifyDanglesMain = self.totPoints[:]
 

    
            #****************************************************************
def isClose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)