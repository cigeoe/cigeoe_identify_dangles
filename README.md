# CIGeoE Identify Dangles

This plugin identifies dangles in a viewport.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_identify_dangles” to folder:

  ```bash
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Identify Dangles”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_identify_dangles” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip”, choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - The plugin is activated clicking on the icon on the toolbar (circle in image 1)

![ALT](/images/image1.png)

2 - Then you have two options available:

- Yes: draw rectangle to identify
- No: identify in viewport

  - If yes option selected, the action will be draw the rectangle

    ![ALT](/images/image2.png)

  - And the result will be:

    ![ALT](/images/image3.png)

  - If no option selected, the result will be

    ![ALT](/images/image4.png)

3 - The plugin is deactivated clicking on the icon on the toolbar (circle in image 1)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
